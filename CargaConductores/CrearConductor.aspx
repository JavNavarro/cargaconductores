﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CrearConductor.aspx.vb" Inherits="CrearConductor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
           
          
            <asp:BulletedList runat="server" ID="result" DisplayMode="Text"  style="font-size:x-large"></asp:BulletedList>

            <div class="col-11">
                  <label runat="server" id="inicio"  style="font-size:x-large">Creacion de Usuario </label>
                <div class="row">
                    <label for="txt_usuario"  style="font-size:x-large">Usuario: </label>
                    <asp:TextBox runat="server" ID="txt_usuario" Text="" CssClass="form-control"  style="font-size:x-large"></asp:TextBox>
                </div>
                <div class="row">
                    <label for="txt_pass"  style="font-size:x-large">Clave: </label>
                    <asp:TextBox runat="server" ID="txt_pass" Text="" CssClass="form-control"  style="font-size:x-large"></asp:TextBox>
                </div>
                <div class="row">
                    <Label for="txt_nombre"  style="font-size:x-large">Nombre: </Label>
                    <asp:TextBox ID="txt_nombre" runat="server" CssClass="form-control"  style="font-size:x-large"></asp:TextBox>
                </div>
                <div class="row"> 
                    <Label for="txt_rut"  style="font-size:x-large">Rut: </Label>
                     <asp:TextBox runat="server" id="txt_rut" CssClass="form-control"  style="font-size:x-large"></asp:TextBox>
                    <asp:Label runat="server" ID="lblguion" Text=" - "  style="font-size:x-large"></asp:Label>
                    <asp:TextBox runat="server" id="txt_dv" CssClass="form-control"  style="font-size:x-large"></asp:TextBox>                    
                </div>
                <div class="row">
                    <Label for="vehiculos" style="font-size:x-large"> Seleccione vehiculo: </Label>
                    <asp:DropDownList runat="server" ID="vehiculos" CssClass="form-control" style="font-size:x-large"></asp:DropDownList>
                </div>
                <div class="row">
                    <asp:Button runat="server" ID="crear" text="crear conductor" class="btn-primary"  style="font-size:x-large"/>
                 </div>
                
            </div>
         
                <div class="col-11">

                    <div class="row">
                        <label runat="server" id="Label1" style="font-size: x-large">Ingresar nuevo vehículo </label>
                        <label for="txt_patente" style="font-size: x-large">Patente</label>
                        <asp:TextBox runat="server" ID="txt_patente" CssClass="form-control" Style="font-size: x-large"></asp:TextBox>
                    </div>
                    <div class="row">
                        <label for="txt_modelo" style="font-size: x-large">Modelo</label>
                        <asp:TextBox ID="txt_modelo" runat="server" CssClass="form-control" Style="font-size: x-large"></asp:TextBox>
                    </div>
                    <div class="row">
                        <label for="txt_capacidad" style="font-size: x-large">Capacidad</label>
                        <asp:TextBox ID="txt_capacidad" runat="server" CssClass="form-control" Style="font-size: x-large"></asp:TextBox>
                    </div>

                    <div class="row">
                        <asp:Button ID="btn_crearVehi" runat="server" Text="ingresar vehiculo" class="btn-primary" Style="font-size: x-large" />
                    </div>
                </div>
            </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5">
                     <label for="ddl_conductores" style="font-size: x-large">Buscar conductor</label>
                      <asp:DropDownList runat="server" ID="ddl_conductores" CssClass="form-control" style="font-size:x-large"></asp:DropDownList>
                </div>
                <div class="col-5">
                      <asp:Button ID="btn_buscarCond" runat="server" Text="Buscar entregas" class="btn-primary" Style="font-size: x-large" />
                </div>
            </div>
        </div>
       

    </form>
</body>
</html>
