﻿
Imports System.Data
Imports System.Drawing

Partial Class listarEntregas
    Inherits System.Web.UI.Page
    Dim conBD As conexionBD = New conexionBD()
    Dim util As Utilidades = New Utilidades()
    Shared flag As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USR") Is Nothing Then
            Response.Redirect(ResolveUrl("Login.aspx"))
        End If
        If Not IsPostBack Then
            flag = 1
            Me.btn_actualizados.Visible = True
            Me.btn_sinVer.Visible = False
            llenarData(flag)


        End If
        Session.Timeout = 720

    End Sub




    Public Sub llenarData(intFlag As Integer)
        Dim idConductor As Int32
        Dim role As String
        Dim dt As DataTable
        idConductor = CInt(Request.QueryString("ID_Usu_conductor"))
        role = Session("USR_role").ToString()

        dt = conBD.llenarEntregasPendientes(idConductor, intFlag)
        Me.lbl_fecha.Text = Date.Now.ToString()
        Me.lbl_user.Text = Session("USR_Nombre").ToString()
        Me.lbl_countGuias.Text = dt.Rows.Count.ToString()
        If dt.Rows.Count > 0 Then
            Me.grd_Pedidos.DataSource = dt
            Me.grd_Pedidos.DataBind()
        End If

        pintarFilas()
    End Sub



    Public Sub pintarFilas()
        For Each fila In grd_Pedidos.Rows
            Dim estado As String = CStr(TryCast(fila.FindControl("estado"), Label).Text)
            If estado = "23" Then
                fila.BackColor = Color.LightGreen
            ElseIf estado = "24" Then
                fila.BackColor = Color.LightPink
            ElseIf estado = "25" Then
                fila.BackColor = Color.LightYellow
            End If
        Next

    End Sub

    Public Sub grd_Pedidos_RowCommand(sender As Object, e As GridViewCommandEventArgs)

        If e.CommandName = "select" Then
            Dim variable As Object = e.CommandArgument
            Dim guia As String = e.CommandArgument.ToString()

            Response.Redirect("ActualizarEstado.aspx?nGuia=" + guia)
        ElseIf e.CommandName = "EnlaceWaze" Then
            Response.Redirect(e.CommandArgument.ToString())
        ElseIf e.CommandName = "EnlaceGoogle" Then
            Response.Redirect(e.CommandArgument.ToString())
        End If

    End Sub





    Protected Sub ruta_Click(sender As Object, e As EventArgs) Handles ruta.Click
        Response.Redirect("ruta.aspx?idConductor=" + Session("ID_Usu").ToString())
    End Sub
    Protected Sub btn_actualizados_Click(sender As Object, e As EventArgs) Handles btn_actualizados.Click
        Me.btn_actualizados.Visible = False
        Me.btn_sinVer.Visible = True
        flag = 2
        llenarData(flag)
    End Sub
    Protected Sub btn_sinVer_Click(sender As Object, e As EventArgs) Handles btn_sinVer.Click
        Me.btn_actualizados.Visible = True
        Me.btn_sinVer.Visible = False
        flag = 1
        llenarData(flag)
    End Sub





End Class
