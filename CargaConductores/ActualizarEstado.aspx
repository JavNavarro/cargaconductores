﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ActualizarEstado.aspx.vb" Inherits="ActualizarEstado" %>

<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    <body>
        <form runat="server" id="panel">
            <div class="container-fluid">
            <div class="row">
                <div class="col-md-11 ml-1">
                    <label for="txt_guia" style="font-size:x-large">Guía</label>
                    <asp:TextBox runat="server" ID="txt_guia" Enabled="false" CssClass="form-control" style="font-size:x-large"></asp:TextBox>
                    </br>
                    </br>
                      <asp:GridView ID="grdGuias" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px"
                   CellPadding="3" CaptionAlign="Bottom" Style="font-size: 12px">
                <Columns>                    
                      <asp:TemplateField HeaderText="Guia" ConvertEmptyStringToNull="False" ItemStyle-CssClass="hidden-xs" HeaderStyle-CssClass="hidden-xs" >
                        <ItemTemplate>
                            <asp:Label ID="guia" Text=<%# Container.DataItem %> runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reportadas" ConvertEmptyStringToNull="False" ItemStyle-CssClass="hidden-xs" HeaderStyle-CssClass="hidden-xs" >
                        <ItemTemplate>
                           <asp:CheckBox ID="chkSelect" CssClass="chkvisible" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>               
                </Columns>
            </asp:GridView>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 ml-1">
                    <label for="ddl_estado" style="font-size:x-large">Estado</label>
                    <asp:DropDownList runat="server" ID="ddl_estado" CssClass="form-control" style="font-size:x-large"></asp:DropDownList> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 ml-1">
                    <label for="fileFoto" style="font-size:x-large">Sacar Foto</label>
                    <input type="file" capture="camera" runat="server" id="fileFoto" >                                      
                </div>
            </div>
            <div class="row">
                <div class="col-lg-11 ml-1">
                    <label for="txt_Obs" style="font-size:x-large">Observación</label>
                    <asp:TextBox runat="server" ID="txt_Obs" CssClass="form-control" style="width:100%;font-size:x-large"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                
                <div class="col-md-6 ml-6">
                    <asp:Button runat="server" ID="btn_aceptar" CssClass="form-control" Text="Aceptar" style="font-size:x-large"/>                    
              </div>
                     <div class="col-md-6 ml-6">
                    <asp:Button runat="server" ID="btnVolver" CssClass="form-control" Text="Volver" style="font-size:x-large"/>                    
              </div>
            </div>
                </div>
        </form>
    </body>    

</html>
