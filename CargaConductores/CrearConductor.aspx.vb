﻿
Imports System.Data

Partial Class CrearConductor
    Inherits System.Web.UI.Page
    Dim conBD As conexionBD = New conexionBD()
    Dim util As Utilidades = New Utilidades()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USR") Is Nothing Then
            Response.Redirect(ResolveUrl("Login.aspx"))
        End If
        If Not IsPostBack Then
            llenarVehiculos()
            llenarConductores()
        End If
        Session.Timeout = 720
    End Sub
    Public Sub llenarVehiculos()
        Dim dt As DataTable
        dt = conBD.listarVehiculos()
        vehiculos.DataSource = dt
        Me.vehiculos.DataTextField = "vehiculo"
        Me.vehiculos.DataValueField = "idVehiculo"
        vehiculos.DataBind()
        vehiculos.Items.Insert(0, "-No definido-")
        vehiculos.SelectedIndex = 0
    End Sub

    Public Sub llenarConductores()
        Dim dt As DataTable
        dt = conBD.ListarConductores()
        ddl_conductores.DataSource = dt
        Me.ddl_conductores.DataTextField = "nombre"
        Me.ddl_conductores.DataValueField = "idConductor"
        ddl_conductores.DataBind()
        ddl_conductores.Items.Insert(0, "-No definido-")
        ddl_conductores.SelectedIndex = 0
    End Sub

    Public Sub crearConductor()
        Dim rut As Int64 = CInt(txt_rut.Text)
        Dim dv As Char = CStr(txt_dv.Text)
        Dim Nombre As String = CStr(txt_nombre.Text)
        Dim vehiculoId As Int32 = vehiculos.SelectedIndex
        Dim usuario As String = CStr(txt_usuario.Text)
        Dim clave As String = CStr(txt_pass.Text)
        Dim creacion As Boolean
        creacion = conBD.crearConductor(rut, dv, Nombre, usuario, clave, vehiculoId)
        If creacion Then
            util.CreateMessageAlert("Conductor creado con exito.")
            limpiarCampos()
            Me.result.Items.Add("Conductor agregado con exito: " + Nombre)
        Else
            util.CreateMessageAlert("Conductor no pudo ser creado, favor validar los datos.")
        End If
    End Sub
    Public Sub limpiarCampos()
        txt_nombre.Text = ""
        txt_dv.Text = ""
        txt_pass.Text = ""
        txt_rut.Text = ""
        txt_usuario.Text = ""
        txt_patente.Text = ""
        txt_modelo.Text = ""
        txt_capacidad.Text = ""
        vehiculos.SelectedIndex = 0
    End Sub
    Public Sub ingresarVehiculo()
        Try


            Dim patente As String = txt_patente.Text
            Dim carga As Double = CDbl(txt_capacidad.Text)
            Dim modelo As String = txt_modelo.Text
            Dim creacion As Boolean
            creacion = conBD.ingresarVehiculo(patente, modelo, carga)
            If creacion Then
                util.CreateMessageAlert("Conductor creado Vehiculo ingresado con exito.")
                limpiarCampos()
                Me.result.Items.Add("Se ingresa vehiculo patente: " + patente)
            Else
                util.CreateMessageAlert("Vehiculo no pudo ser ingresado, favor validar los datos.")
            End If
        Catch ex As Exception

        End Try
    End Sub





    Protected Sub crear_Click(sender As Object, e As EventArgs) Handles crear.Click
        crearConductor()
    End Sub
    Protected Sub btn_crearVehi_Click(sender As Object, e As EventArgs) Handles btn_crearVehi.Click
        ingresarVehiculo()
    End Sub

    Protected Sub btn_buscarCond_Click(sender As Object, e As EventArgs) Handles btn_buscarCond.Click
        If Me.ddl_conductores.SelectedIndex = 0 Then
            util.CreateMessageAlert("Debe seleccionar un conductor para esta opcion")
        Else
            If Session("USR").ToString().Contains("dministrador") Then
                Response.Redirect("adminEntregas.aspx?ID_Usu_conductor=" + Me.ddl_conductores.SelectedIndex.ToString())
            Else
                Response.Redirect("listarEntregas.aspx?ID_Usu_conductor=" + Me.ddl_conductores.SelectedIndex.ToString())
            End If

        End If
    End Sub
End Class
