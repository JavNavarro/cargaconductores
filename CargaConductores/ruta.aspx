﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="ruta.aspx.vb" Inherits="ruta" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Google Map</title>

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAqkmIqgeZ91N8vexk-MiH-qD2YADM_NH4"></script>

    <script src="Scripts/jquery-1.9.1.min.js" type="text/javascript"></script>

    <script src="Scripts/googlemap.js" type="text/javascript"></script>

    <script src="Scripts/jquery.tablednd.js" type="text/javascript"></script>


    <style type="text/css">
        #spinner { display:none; } 
        body.busy .spinner { display:block !important; }
    </style>


</head>
<body>
    <div class="col-6">
        <button type="button" name="btn-enviar" class="btn btn-primary w-100">
        <span class="spinner spinner-border spinner-border-sm mr-3" id="spinner" role="status" aria-hidden="true">
        </span>Enviar</button>
    </div>
    <form id="form1" runat="server">
    <div id="dvMap" style="height: 600px; width: 80%;">
    </div>
    <button id="btnAgregar" type="button">Click Me!</button>
    <script type="text/javascript" language="javascript">
        InitializeMap();
    </script>

    </form>
</body>
</html>
