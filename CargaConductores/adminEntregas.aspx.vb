﻿
Imports System.Data

Partial Class adminEntregas
    Inherits System.Web.UI.Page
    Dim conBD As conexionBD = New conexionBD()
    Dim util As Utilidades = New Utilidades()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USR") Is Nothing Then
            Response.Redirect(ResolveUrl("Login.aspx"))
        End If
        If Not IsPostBack Then
            llenarData()
        End If
        Session.Timeout = 720

    End Sub

    Public Sub llenarData()
        Dim idConductor As Int32

        Dim dt As DataTable
        idConductor = CInt(Request.QueryString("ID_Usu_conductor"))
        dt = conBD.llenarEntregasAdmin(idConductor)
        Me.lbl_countGuias.Text = dt.Rows.Count.ToString()
        If dt.Rows.Count > 0 Then
            Me.grd_Pedidos.DataSource = dt
            Me.grd_Pedidos.DataBind()
        End If

    End Sub

    Public Sub grd_Pedidos_RowCommand(sender As Object, e As GridViewCommandEventArgs)

        If e.CommandName = "select" Then
            Dim variable As Object = e.CommandArgument
            Dim guia As String = e.CommandArgument.ToString()

            Response.Redirect("ActualizarEstado.aspx?nGuia=" + guia)
        ElseIf e.CommandName = "eliminar" Then
            Dim variable As Object = e.CommandArgument
            Dim guia As String = e.CommandArgument.ToString()
            conBD.eliminarPunto(guia)
            llenarData()
        End If

    End Sub





End Class
