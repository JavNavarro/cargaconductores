﻿


Partial Class Login
    Inherits System.Web.UI.Page
    Dim conBD As conexionBD = New conexionBD()
    Dim util As Utilidades = New Utilidades()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Function validateUser() As Boolean
        Session("USR_Nombre") = Nothing
        Session("USR_role") = Nothing
        Session("USR") = Nothing
        Session("ID_Usu") = Nothing
        Session("N_Empresa") = Nothing
        Dim result As Boolean = False
        Dim user As Usuario = conBD.getLogin(Me.user.Text, Me.pass.Text)
        If Not (user Is Nothing) Then
            Session("USR_Nombre") = user.Nombre
            Session("USR_role") = user.Role
            Session("USR") = user.user
            Session("ID_Usu") = user.ID
            Session("N_Empresa") = user.Nombre
            result = True
        End If
        Return result
    End Function

    Protected Sub btn_enviar_Click(sender As Object, e As EventArgs) Handles btn_enviar.Click
        Try
            validateUser()
            If Session("USR").ToString().Length > 0 Then
                If Not Session("USR_role") Is Nothing Then
                    If Session("USR_role") = "Conductor" Then
                        Response.Redirect("PuntosDeEntrega.aspx")
                    Else
                        Response.Redirect("CrearConductor.aspx")
                    End If
                Else
                        util.CreateMessageAlert("No es posible el ingreso, intentelo nuevamente")
                End If
            Else
            End If
        Catch ex As Exception
            util.CreateMessageAlert("Error: " + ex.Message)
        End Try
    End Sub


End Class
