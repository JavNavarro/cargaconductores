﻿
Imports System.Data
Imports System.IO
Imports System.Net

Partial Class ActualizarEstado
    Inherits System.Web.UI.Page
    Dim conBD As conexionBD = New conexionBD()
    Dim util As Utilidades = New Utilidades()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USR") Is Nothing Then
            Response.Redirect(ResolveUrl("Login.aspx"))
        End If
        If Not IsPostBack Then
            llenarDdlEstados()
            llenarInfo()
        End If
        Session.Timeout = 720
    End Sub
    Public Sub llenarInfo()

        Me.txt_guia.Text = Request.QueryString("nGuia")
        Session("nGuia") = Me.txt_guia.Text
        Dim listGuias As String()
        listGuias = (Me.txt_guia.Text).Split(",")
        ' listGuias.Add((Me.txt_guia.Text).Split(","))
        grdGuias.DataSource = listGuias
        grdGuias.DataBind()


    End Sub
    Public Sub llenarDdlEstados()
        Dim dt As DataTable
        dt = conBD.traerEstados()
        Me.ddl_estado.DataSource = dt
        Me.ddl_estado.DataTextField = "detalleEstado"
        Me.ddl_estado.DataValueField = "idEstado"
        Me.ddl_estado.DataBind()
    End Sub
    Protected Sub btn_aceptar_Click(sender As Object, e As EventArgs) Handles btn_aceptar.Click
        Dim respuesta As Boolean

        Try
            My.Computer.FileSystem.CreateDirectory("c:\EvidenciasEntregas")
        Catch ex As Exception
            Dim err As String = ex.ToString()
        End Try
        Dim ruta As String = "c:\EvidenciasEntregas\" + Me.txt_guia.Text + "_" + Me.ddl_estado.SelectedValue + ".jpg"

        If (Request.Files("fileFoto").FileName.ToString().Equals("")) Then
            ruta = "no se agrega foto."
        Else
            Request.Files("fileFoto").SaveAs(ruta)
        End If


        Dim obser As String = ""
        If Not Me.txt_Obs.Text = "" Then
            obser = Me.txt_Obs.Text
        End If
        Dim GuiaEnviar As String = ""
        Dim totalGuias As String = ""
        Dim notificar As Boolean
        Dim conteoGuias As Int32 = 0
        For Each Fila As GridViewRow In grdGuias.Rows
            Dim chek As CheckBox = Fila.FindControl("chkSelect")
            If chek.Checked Then
                conteoGuias += 1
                GuiaEnviar = CStr(TryCast(Fila.FindControl("guia"), Label).Text)
                respuesta = conBD.actualizarEstado(GuiaEnviar, ddl_estado.SelectedValue.ToString(), ruta, obser) 'app.shipex.cl/cargaConductores/PuntosDeEntrega.aspx
                Try
                    conBD.guardarHistorialCarga(CLng(GuiaEnviar), 28, "Se actualiza por conductor " & Session("USR_Nombre").ToString() & "a estado: " & Me.ddl_estado.Text.ToString())

                Catch ex As Exception

                End Try
                totalGuias = totalGuias & GuiaEnviar & ","
            End If

        Next
        If conteoGuias = 0 Then
            util.CreateMessageAlert("Favor seleccione al menos una guia para cambiar estado.")
            Exit Sub
        End If


        If respuesta Then
            util.ShowMessageAndRedirect("guia(s): " + totalGuias + "  actualizada(s) con exito", "PuntosDeEntrega.aspx")
            If Me.ddl_estado.SelectedValue.ToString() = 23 Then
                notificar = conBD.preguntarCorreoTransaccional(Convert.ToInt64(GuiaEnviar))
                If notificar Then
                    ' JD : Servicio para envio de correos con estado En transito / Entregado
                    Utilidades.EnvioCorreoMegaEstados(Convert.ToInt64(GuiaEnviar))


                End If

                enviarIngresoCorreo(GuiaEnviar, totalGuias)


            End If
        Else
            util.CreateMessageAlert("No se pudo actualizar estado.")
        End If


    End Sub


    Public Sub enviarIngresoCorreo(guia As String, totalGuias As String)

        'Dim shipexBD As ShipexBD = New ShipexBD()
        Dim jsonString As String = ""
        Dim emailCliente As String = ""
        Dim sistema As String = ""
        Dim nombreCLiente As String = ""

        Dim datosCliente As List(Of String) = conBD.Get_mail(guia)
        Try


            emailCliente = datosCliente.Item(0)
            nombreCLiente = datosCliente.Item(1)
            sistema = datosCliente.Item(2)
            Dim enviadoNotificacion As Boolean = conBD.preguntarNotificacion(guia)
            Dim configId As String = conBD.Get_configMail(sistema)
            Dim correoEmpresa As String = conBD.emailEmpresa(guia)
            If correoEmpresa.Length > 2 Then
                If enviadoNotificacion Then
                    If ((correoEmpresa.Contains("@")) And (correoEmpresa.Contains("."))) Then
                        correoEmpresa = quitarCaracteresEspecialesSql(correoEmpresa)
                        If Not (configId.Equals("")) Then
                            Dim codDespacho As String = ""
                            Dim ReferenciaCliente As String = ""
                            codDespacho = conBD.buscarCodDespachoConGuia(guia)
                            ReferenciaCliente = conBD.buscarRefereenciaCLienteConDespacho(codDespacho)
                            jsonString = "[{" + """para"":""" & correoEmpresa & ""","
                            jsonString = jsonString & """sistema"":""" & sistema & ""","
                            jsonString = jsonString & """configId"": " & configId & ","
                            jsonString = jsonString & """valVariable"": ""{'@@DESPACHO':'" & codDespacho & "','@@GUIA_NUMBER':'" & totalGuias.Replace(",", "-")
                            '                                               @@DESPACHO
                            jsonString = jsonString & ",'@@REFERENCIA':'" & ReferenciaCliente & "'}" & """}]"
                            agregarEnvio(jsonString)
                        Else
                            SaveLog2("   " & guia & "sin informacion de cliente")
                        End If
                    Else
                        SaveLog2("   " & correoEmpresa & "no valido para guia " + guia)
                    End If
                Else
                    SaveLog2("guia ya con correo enviado se reintento:  " + guia)
                End If
            Else
                If enviadoNotificacion Then
                    If ((emailCliente.Contains("@")) And (emailCliente.Contains("."))) Then
                        emailCliente = quitarCaracteresEspecialesSql(emailCliente)
                        If Not (configId.Equals("")) Then
                            jsonString = "[{" + """para"":""" & emailCliente & ""","
                            jsonString = jsonString & """sistema"":""" & sistema & ""","
                            jsonString = jsonString & """configId"": " & configId & ","
                            jsonString = jsonString & """valVariable"": ""{'@@nombre':'" & nombreCLiente & "'}" & """}]"
                            agregarEnvio(jsonString)
                        Else
                            SaveLog2("   " & guia & "sin informacion de cliente")
                        End If
                    Else
                        SaveLog2("   " & emailCliente & "no valido para guia " + guia)
                    End If
                Else
                    SaveLog2("guia ya con correo enviado se reintento:  " + guia)
                End If
            End If

        Catch ex As Exception
            Dim valErr As String = ex.ToString()
        End Try
    End Sub
    Private Sub agregarEnvio(json As String)
        Try

            Dim URL As String = "http://app.shipex.cl/EnvioCorreo/api/Envios"
            Dim postData As String = json
            Dim Bts As Byte() = Encoding.UTF8.GetBytes(postData)
            Dim httpWebRequest1 As WebRequest = WebRequest.Create(URL)
            httpWebRequest1.Method = "POST"
            httpWebRequest1.ContentLength = Bts.Length
            httpWebRequest1.ContentType = "text/json"
            Using requestStream As Stream = httpWebRequest1.GetRequestStream()
                requestStream.Write(Bts, 0, Bts.Count())
            End Using
            Dim respon_se As WebResponse = httpWebRequest1.GetResponse()
            Dim dataStream As Stream = respon_se.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            Dim ValLenght As Integer = responseFromServer.Length - 2
            Dim ValPreFinal As String = responseFromServer.Substring(1, ValLenght)
            Dim Final As String = ValPreFinal.Replace("\", String.Empty)

            reader.Close()
            respon_se.Close()

        Catch ex As Exception
            Dim errorStr As String = ex.ToString
        End Try


    End Sub

    Public Sub SaveLog2(ByVal Message1 As String)
        Dim outputFile2 As System.IO.StreamWriter
        Try
            outputFile2 = New System.IO.StreamWriter("C:\Nueva carpeta\cargaConductores.txt", True)
            outputFile2.WriteLine("--------------------------------------------------------------------------")
            outputFile2.WriteLine("Date : " & Now.ToString)
            outputFile2.WriteLine(Message1)
            outputFile2.WriteLine("---------------------------------------------------------------------------")
            outputFile2.Close()
        Catch ex As Exception

        End Try

    End Sub
    Public Function quitarCaracteresEspecialesSql(palabra As String) As String
        Return palabra.Replace("'", "").Replace("#", "").Replace("%", "").Replace("!", "").Replace("$", "").Replace("&", "")
    End Function


    Protected Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Response.Redirect("PuntosDeEntrega.aspx")
    End Sub
End Class
