﻿


//You can calculate directions (using a variety of methods of transportation) by using the DirectionsService object.
var directionsService = new google.maps.DirectionsService();

//Define a variable with all map points.
var _mapPoints = new Array();

//Define a DirectionsRenderer variable.
var _directionsRenderer = '';

//This will give you the map zoom value.
var zoom_option = 12;

//LegPoints is your route points between two locations.
var LegPoints = new Array();

//Google map object
var map;

//Se inicia en Shipex el mapa (coquimbo 1231)
var myCenter = new google.maps.LatLng(-33.4571205, -70.6534208);

//InitializeMap() function is used to initialize google map on page load.
function InitializeMap() {
    $('body').addClass('busy');
    //DirectionsRenderer() is a used to render the direction
    _directionsRenderer = new google.maps.DirectionsRenderer();

    //Set the your own options for map.
    var myOptions = {
        zoom: zoom_option,
        zoomControl: true,
        center: myCenter,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    //Define the map.
    map = new google.maps.Map(document.getElementById("dvMap"), myOptions);

    
    //Se inicializa el marcador en el centro Shipex definido en el objeto myCenter
    /*var marker = new google.maps.Marker({
        position: myCenter,
    });
    marker.setMap(map);*/
    //Set the map for directionsRenderer
    _directionsRenderer.setMap(map);
    //Set different options for DirectionsRenderer mehtods.
    //draggable option will used to drag the route.
    _directionsRenderer.setOptions({
        draggable: true
    });
    
    //Seteo la capa de trafico
    var trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(map);


    //Cierro carga de mapa
    $('body').removeClass('busy');

    //Add the doubel click event to map.
    /*
    google.maps.event.addListener(map, "click", function(event) {
        var _currentPoints = event.latLng;
        _mapPoints.push(_currentPoints);
        LegPoints.push('');
        getRoutePointsAndWaypoints(_mapPoints);
    });
    */
    //Add the directions changed event to map.
    /*
    google.maps.event.addListener(_directionsRenderer, 'directions_changed', function () {
        var myroute = _directionsRenderer.directions.routes[0];
        CreateRoute(myroute);
        zoom_option = map.getZoom();
    });*/
}


function CreateRoute(myroute) {

    var index = 0;
    if (_mapPoints.length > 10) {
        index = _mapPoints.length - 10;
    }

    for (var i = 0; i < myroute.legs.length; i++) {
        saveLegPoints(myroute.legs[i], index);
        index = index + 1;
    }
}

//Saving the all the legs points between two routes
function saveLegPoints(leg, index) {
    var points = new Array();
    for (var i = 0; i < leg.steps.length; i++) {
        for (var j = 0; j < leg.steps[i].lat_lngs.length; j++) {
            points.push(leg.steps[i].lat_lngs[j]);
        }
    }
    LegPoints[index] = points;
}

//This will draw the more then 10 points route on map.
function drawPreviousRoute(Legs) {
    var segPointValue = new Array();
    for (var i = 0; i < Legs; i++) {
        var innerArry = LegPoints[i];
        for (var j = 0; j < innerArry.length; j++) {
            segPointValue.push(innerArry[j]);
        }
        addPreviousMarker(innerArry[0]);
    }
    var polyOptions = {
        path: segPointValue,
        strokeColor: '#9D4DBF',
        strokeWeight: 3
    };
    var poly = new google.maps.Polyline(polyOptions);
    poly.setMap(map);
}

//This wil add the marker icon to the route.
function addPreviousMarker(myLatlng) {
    var marker = new google.maps.Marker({
        position: myLatlng,
        icon: "Images/red-circle.png",
        title: ""
    });
    marker.setMap(map);
}

//getRoutePointsAndWaypoints() will help you to pass points and waypoints to drawRoute() function
function getRoutePointsAndWaypoints(Points) {
    if (Points.length <= 100) {
        drawRoutePointsAndWaypoints(Points);
    }
    else {
        var newPoints = new Array();
        var startPoint = Points.length - 10;
        var Legs = Points.length - 10;
        for (var i = startPoint; i < Points.length; i++) {
            newPoints.push(Points[i]);
        }
        drawRoutePointsAndWaypoints(newPoints);
        drawPreviousRoute(Legs);
    }
}

function drawRoutePointsAndWaypoints(Points) {
    //Define a variable for waypoints.
    var _waypoints = new Array();

    if (Points.length > 2) //Waypoints will be come.
    {
        for (var j = 1; j < Points.length - 1; j++) {
            var address = Points[j];
            if (address !== "") {
                _waypoints.push({
                    location: address,
                    stopover: true  //stopover is used to show marker on map for waypoints
                });
            }
        }
        //Call a drawRoute() function
        drawRoute(Points[0], Points[Points.length - 1], _waypoints);
    } else if (Points.length > 1) {
        //Call a drawRoute() function only for start and end locations
        drawRoute(Points[_mapPoints.length - 2], Points[Points.length - 1], _waypoints);
    } else {
        //Call a drawRoute() function only for one point as start and end locations.
        drawRoute(Points[_mapPoints.length - 1], Points[Points.length - 1], _waypoints);
    }
}

//drawRoute() will help actual draw the route on map.
function drawRoute(originAddress, destinationAddress, _waypoints) {
    //Define a request variable for route .
    var _request = '';

    //This is for more then two locatins
    if (_waypoints.length > 0) {
        _request = {
            origin: originAddress,
            destination: destinationAddress,
            waypoints: _waypoints, //an array of waypoints
            optimizeWaypoints: true, //set to true if you want google to determine the shortest route or false to use the order specified.
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
    } else {
        //This is for one or two locations. Here noway point is used.
        _request = {
            origin: originAddress,
            destination: destinationAddress,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
    }

    //This will take the request and draw the route and return response and status as output
    directionsService.route(_request, function(_response, _status) {
        if (_status == google.maps.DirectionsStatus.OK) {
            _directionsRenderer.setDirections(_response);
        }
        else if (_status == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
            //Al sobrepasar los 10 registros entrega OVER_QUERY_LIMIT, por esto se espera un cant. determinada de seg para volver a cargar.
            setTimeout(1000); 
            drawRoute(originAddress, destinationAddress, _waypoints);
        }
    });
}

function getLatitudeAndLongitudeByAddress(_direccion) {
    //_direccion = '353+Santa+Isabel,Santiago,Chile';
    var url = "https://maps.googleapis.com/maps/api/geocode/json?address=direccion&key=AIzaSyAqkmIqgeZ91N8vexk-MiH-qD2YADM_NH4";
    url = url.replace("direccion", _direccion);
    var result = null;
    $.ajax({
        url: url,
        async:false,
        success: function (respuesta) {
                           // console.log(respuesta);
                            if (respuesta.status === "OK") {
                                //Es necesario solo "pintar" direcciones exactas, por eso si retorna mas de un resultado, se elimina
                                if (respuesta.results.length == 1) {
                                    result = { lat : respuesta.results[0].geometry.location.lat, lng : respuesta.results[0].geometry.location.lng };
                                }
                                else {
                                    result = { lat: respuesta.results[0].geometry.location.lat, lng: respuesta.results[0].geometry.location.lng };
                                }
                            }
        },
        error: function () {
            console.log("No se ha podido obtener la información desde geocode");
        }
    });
    return result;
}

$(document).ready(function () {
     ModalTrackingByURL();
    //$("#btnAgregar").click(function () {
    //    $('body').addClass('busy');
    //    var direccionesPrueba = [
    //        "Av Mexico 183, Recoleta, Chile",
    //        "Santa isabel 353, Santiago, Chile", 
    //        "Coquimbo 1201, Santiago, Chile",
    //        "El Olimpo 1313, Maipu, Chile",
    //        "Padre Hurtado Central 1531, Las Condes, Chile",
    //        "Las margaritas 807, Lo Prado, Chile",
    //        "Carlos antunez 1920, Providencia, Chile",
    //        "Domingo Faustino Sarmiento 358, Ñuñoa, Chile",
    //        "La candelaria 2405, Maipu, Chile",
    //        "Eleuterio ramirez 1024, Santiago, Chile",
    //        "Maria Rozas Velasquez 51, Estacion Central, Chile",
    //        "San Francisco 335"     
    //    ];
       

    //    for (var i = 0; i < direccionesPrueba.length; i++) {
    //        var res = getLatitudeAndLongitudeByAddress(direccionesPrueba[i]);
    //        //Set punto
    //        var _currentPoints = new google.maps.LatLng(res.lat, res.lng);
    //        _mapPoints.push(_currentPoints);
    //        LegPoints.push('');
    //     //   getRoutePointsAndWaypoints(_mapPoints);
           
    //    }

    //    drawRoutePointsAndWaypoints(_mapPoints);
    //    $('body').removeClass('busy');
    //});
});


function ModalTrackingByURL() {
    ////alert("cualquier wea!");

    $.ajax({
        type: "POST",
        url: "ruta.aspx/validarGuia",        
        contentType: "Application/json; charset=utf-8",  
        
        async: false,
        success: function (respuesta) {
            ////alert("suxes");  
           // var dirArray = split("||",respuesta) 
            //var direccionesPrueba = respuesta


           // $("#btnAgregar").click(function () {
                $('body').addClass('busy');
                //var direccionesPrueba = [
                //    "Av Mexico 183, Recoleta, Chile",
                //    "Santa isabel 353, Santiago, Chile",
                //    "Coquimbo 1201, Santiago, Chile",
                //    "El Olimpo 1313, Maipu, Chile",
                //    "Padre Hurtado Central 1531, Las Condes, Chile",
                //    "Las margaritas 807, Lo Prado, Chile",
                //    "Carlos antunez 1920, Providencia, Chile",
                //    "Domingo Faustino Sarmiento 358, Ñuñoa, Chile",
                //    "La candelaria 2405, Maipu, Chile",
                //    "Eleuterio ramirez 1024, Santiago, Chile",
                //    "Maria Rozas Velasquez 51, Estacion Central, Chile",
                //    "San Francisco 335"
                //];
            var direccionesPrueba = respuesta.d;

            for (var i = 0; i < direccionesPrueba.length; i++) {
                try {


                        var res = getLatitudeAndLongitudeByAddress(direccionesPrueba[i]);
                        //Set punto
                        var _currentPoints = new google.maps.LatLng(res.lat, res.lng);

                        _mapPoints.push(_currentPoints);
                        LegPoints.push('');
                        //   getRoutePointsAndWaypoints(_mapPoints);
                    } catch (error) {
                   // console.error(error);//pasar direccion a front
                    }
                }

                drawRoutePointsAndWaypoints(_mapPoints);
                $('body').removeClass('busy');
            //});
            
        },
        failure: function (response) {
            alert("Error al cargar, comuniquese con el administrador.");

        }
    });

}

