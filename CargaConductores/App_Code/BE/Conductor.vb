﻿Imports Microsoft.VisualBasic

Public Class Conductor
    Private _id As String
    Public Property id() As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property
    Private _nombre As String
    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property
    Private _rut As Int64
    Public Property rut() As Int64
        Get
            Return _rut
        End Get
        Set(value As Int64)
            _rut = value
        End Set
    End Property
    Private _digitoVer As Char
    Public Property digitoVer() As Char
        Get
            Return _digitoVer
        End Get
        Set(value As Char)
            _digitoVer = value
        End Set
    End Property
    Private _estado As Int32
    Public Property estado() As Int32
        Get
            Return _estado
        End Get
        Set(value As Int32)
            _estado = value
        End Set
    End Property


End Class
