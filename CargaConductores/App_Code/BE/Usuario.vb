﻿Imports Microsoft.VisualBasic

Public Class Usuario
    Private _id As Integer
    Public Property ID() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Private _nombre As String
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property
    Private _role As String
    Public Property Role() As String
        Get
            Return _role
        End Get
        Set(value As String)
            _role = value
        End Set
    End Property
    Private _clave As String
    Public Property clave() As String
        Get
            Return _clave
        End Get
        Set(value As String)
            _clave = value
        End Set
    End Property
    Private _user As String
    Public Property user() As String
        Get
            Return _user
        End Get
        Set(value As String)
            _user = value
        End Set
    End Property


End Class
