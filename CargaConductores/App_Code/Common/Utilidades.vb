﻿Imports System.Net
Imports Microsoft.VisualBasic

Public Class Utilidades
    Public Function quitarCaracteresEspecialesSql(palabra As String) As String
        Return palabra.Replace("'", "").Replace("#", "").Replace("%", "").Replace("!", "").Replace("$", "").Replace("&", "")
    End Function
    Public Sub CreateMessageAlert(ByVal strMessage As String)
        Dim guidKey As Guid = Guid.NewGuid()
        Dim pg As Page = HttpContext.Current.Handler
        Dim strScript As String = "alert('" & strMessage & "');"
        pg.ClientScript.RegisterStartupScript(pg.GetType(), guidKey.ToString(), strScript, True)
    End Sub
    Public Function quitarAcentos(frase As String) As String
        Return frase.Replace("á", "a").Replace("Á", "A").Replace("é", "e").Replace("É", "E").Replace("Í", "I").Replace("í", "i").Replace("ó", "o").Replace("Ó", "O").Replace("Ú", "U").Replace("ú", "u")
    End Function
    Public Shared Sub ShowMessageAndRedirect(ByVal message As String, ByVal lpRedirectPage As String)
        Dim cleanMessage As String = message
        Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
        Dim script As String = String.Format("alert('{0}');", cleanMessage)
        script += " window.location.href='" & lpRedirectPage & "';"

        If page IsNot Nothing AndAlso Not page.ClientScript.IsClientScriptBlockRegistered("alert") Then
            page.ClientScript.RegisterClientScriptBlock(page.[GetType](), "alert", script, True)
        End If
    End Sub

    Public Shared Sub EnvioCorreoMegaEstados(ByVal guia As Int64)
        Using wc As New WebClient()
            wc.Headers(HttpRequestHeader.ContentType) = "application/x-www-form-urlencoded"
            Dim htmlResult As String = wc.UploadString("http://eshopex.cl/CorreoMegaEstados/CorreosMegaEstados.aspx?guia=" & guia & "", "")
        End Using
    End Sub

End Class
