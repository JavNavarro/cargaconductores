﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
'Imports DatosConstantesDespachos
Imports System.Collections.Generic
Imports System
Public Class conexionBD
    Dim StringConexion As String = ConfigurationManager.AppSettings.Get("conexionShipex").ToString()
    Public Function getLogin(idUser As String, clave As String) As Usuario
        Dim usuario As New Usuario
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("loginConductores", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("user", idUser)
            cmd.Parameters.AddWithValue("clave", clave)
            Dim resultUsuario As SqlDataReader = cmd.ExecuteReader
            While resultUsuario.Read
                usuario.Nombre = resultUsuario("nombre")
                usuario.Role = resultUsuario("Role")
                usuario.ID = resultUsuario("ID")
                usuario.user = idUser
                usuario.clave = clave
            End While
        End Using
        Return usuario
    End Function
    Public Function preguntarCorreoTransaccional(guia As String) As Boolean

        Dim cantidad As Int32 = 0
        Try

            'Dim cmd As SqlCommand = Nothing

            Dim result As SqlDataReader
            Using cnn As New SqlConnection(StringConexion)
                cnn.Open()
                Dim cmd As SqlCommand
                cmd = New SqlCommand("Shipex_preguntarNotificacionTransaccional", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("guia", guia))
                cmd.Parameters.Add(New SqlParameter("megaestado", 3))
                result = cmd.ExecuteReader()

                While result.Read
                    cantidad = result("cantidad").ToString
                End While
                cnn.Close()
                cnn.Dispose()

            End Using

        Catch ex As Exception

        End Try
        If cantidad > 1 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub guardarHistorialCarga(guia As Long, userId As Int32, hito As String)
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As SqlCommand
            cmd = New SqlCommand("guardar_historialCarga", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("user", userId))
            cmd.Parameters.Add(New SqlParameter("guia", guia))
            cmd.Parameters.Add(New SqlParameter("hito", hito))
            cmd.ExecuteNonQuery()
            cnn.Close()
        End Using
    End Sub

    Public Function llenarEntregasPendientes(cliente As Int32, flag As Int32) As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            ' Dim cmd As New SqlCommand("Shipex_traerEntregasConductor", cnn)
            Dim cmd As New SqlCommand("Shipex_traerRutaConductor", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("idConductor", cliente))
            cmd.Parameters.Add(New SqlParameter("flag", flag))
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function
    Public Function llenarEntregasAdmin(cliente As Int32) As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            ' Dim cmd As New SqlCommand("Shipex_traerEntregasConductor", cnn)
            Dim cmd As New SqlCommand("Shipex_traerRutaAdministrador", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("idConductor", cliente))
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function

    Public Function cargarRuteo(cliente As Int32) As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("Shipex_traerRutaConductor", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("idConductor", cliente))
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function


    Public Function buscarEntregas(cliente As Int32) As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("mostrarCargaSegunFecha", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("idConductor", cliente))
            'cmd.Parameters.Add(New SqlParameter("fecha", fecha))
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function
    Public Function Get_mail(guia As String) As List(Of String)
        Dim email As String = ""
        Dim nombre As String = ""
        Dim empresa As String = ""
        Dim resultado As New List(Of String)
        Try
            Using cnn As New SqlConnection(StringConexion)
                Dim result As SqlDataReader

                cnn.Open()
                Dim cmd As SqlCommand
                cmd = New SqlCommand("Shipex_obtenerMail", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("guia", guia))
                result = cmd.ExecuteReader()
                While result.Read
                    email = result("email").ToString
                    nombre = result("nombreCliente").ToString
                    empresa = result("clienteEmpresa").ToString
                End While
                resultado.Add(email)
                resultado.Add(nombre)
                resultado.Add(empresa)
                cnn.Close()
            End Using
        Catch
        End Try
        Return resultado
    End Function

    Public Function Get_configMail(empresa As String) As String

        Dim config As String = ""
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As SqlCommand

            cmd = New SqlCommand("Shipex_obtenerClientesParaEnvioCorreo", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("cliente", empresa))
            Dim reader As SqlDataReader = cmd.ExecuteReader
            While reader.Read
                config = reader("idConfig").ToString
            End While
            reader.Close()
        End Using

        Return config

    End Function
    Public Function emailEmpresa(guia As Int64) As String

        Dim mail As String = ""
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As SqlCommand

            cmd = New SqlCommand("preguntarNotificacionAClienteShipex", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("awb", guia))
            Dim reader As SqlDataReader = cmd.ExecuteReader
            While reader.Read
                mail = reader("correo").ToString
            End While
            reader.Close()
        End Using

        Return mail

    End Function

    Public Function buscarCodDespachoConGuia(guia As Int64) As String

        Dim cod As String = ""
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As SqlCommand

            cmd = New SqlCommand("CodDespachoSegunGuia", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("awb", guia))
            Dim reader As SqlDataReader = cmd.ExecuteReader
            While reader.Read
                cod = reader("coddespacho").ToString
            End While
            reader.Close()
        End Using

        Return cod

    End Function

    Public Function buscarRefereenciaCLienteConDespacho(coddespacho As Int64) As String

        Dim refcli As String = ""
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As SqlCommand

            cmd = New SqlCommand("referenciaClienteSegunCodDespacho", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("coddespacho", coddespacho))
            Dim reader As SqlDataReader = cmd.ExecuteReader
            While reader.Read
                refcli = reader("referenciaCliente").ToString
            End While
            reader.Close()
        End Using

        Return refcli

    End Function


    Public Function preguntarNotificacion(guia As String) As Boolean

        Dim cantidad As Int32 = 0
        'Dim cmd As SqlCommand = Nothing

        Dim result As SqlDataReader
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As SqlCommand
            cmd = New SqlCommand("Shipex_preguntarNotificacion", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("guia", guia))
            result = cmd.ExecuteReader()

            While result.Read
                cantidad = result("cantidad").ToString
            End While
            cnn.Close()
            cnn.Dispose()
        End Using
        If cantidad > 1 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function listarVehiculos() As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("Shipex_traerVehiculos", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function
    Public Function ListarConductores() As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("ListarConductores", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function

    Public Function crearConductor(rut As Int64, dv As Char, Nombre As String, usuario As String, clave As String, vehiculoId As Int32) As Boolean
        Dim respuesta As Boolean = False
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("shipex_crearConductor", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("rut", rut))
            cmd.Parameters.Add(New SqlParameter("dv", dv))
            cmd.Parameters.Add(New SqlParameter("nombre", Nombre))
            cmd.Parameters.Add(New SqlParameter("user", usuario))
            cmd.Parameters.Add(New SqlParameter("clave", clave))
            cmd.Parameters.Add(New SqlParameter("idvehiculo", vehiculoId))
            'Dim result As SqlDataReader = cmd.ExecuteReader
            Dim cantFilas As Int32
            cantFilas = cmd.ExecuteNonQuery()
            If cantFilas > 0 Then
                respuesta = True
            End If
            cnn.Close()
        End Using
        Return respuesta
    End Function

    Public Function ingresarVehiculo(patente As String, modelo As String, carga As Double) As Boolean
        Dim respuesta As Boolean = False
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("shipex_crearVehiculo", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("modelo", modelo))
            cmd.Parameters.Add(New SqlParameter("carga", carga))
            cmd.Parameters.Add(New SqlParameter("patente", patente))
            Dim result As SqlDataReader = cmd.ExecuteReader
            Dim cantFilas As Int32
            cantFilas = cmd.ExecuteNonQuery()
            If cantFilas > 0 Then
                respuesta = True
            End If
            cnn.Close()
        End Using
        Return respuesta
    End Function
    Public Sub eliminarPunto(guia As String)
        Dim listGuias As String()
        listGuias = guia.Split(",")
        For Each a In listGuias
            Dim respuesta As Boolean = False
            Using cnn As New SqlConnection(StringConexion)
                cnn.Open()
                Dim cmd As New SqlCommand("shipex_eliminarPunto", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("guia", a))
                cmd.ExecuteNonQuery()

                cnn.Close()
            End Using
        Next

    End Sub


    Public Function traerEstados() As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("traerEstadosEntregasOtros", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function

    Public Function actualizarEstado(guia As String, idEstado As String, ruta As String, obs As String) As Boolean
        Dim cantFilas As Int32
        Dim respuesta As Boolean = False
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("ingresarEstadoCargaConductor", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("guia", guia))
            cmd.Parameters.Add(New SqlParameter("idEstado", idEstado))
            cmd.Parameters.Add(New SqlParameter("url_entrega", ruta))
            cmd.Parameters.Add(New SqlParameter("obs", obs))
            cantFilas = cmd.ExecuteNonQuery()
            If cantFilas > 0 Then
                respuesta = True
            End If
            cnn.Close()
        End Using
        Return respuesta
    End Function

    Public Function GetIdConductor(usr As String) As Int32

        Dim idCond As Int32 = 0
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As SqlCommand

            cmd = New SqlCommand("Shipex_traerIDConductor", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("usr", usr))
            Dim reader As SqlDataReader = cmd.ExecuteReader
            While reader.Read
                idCond = CInt(reader("idConductor").ToString)
            End While
            reader.Close()
        End Using

        Return idCond

    End Function

    Public Function Shipex_traerRutaConductorPorFiltro(cliente As Int32, flag As Int32, filtro As String) As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("Shipex_traerRutaConductorPorFiltro", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("idConductor", cliente))
            cmd.Parameters.Add(New SqlParameter("flag", flag))
            cmd.Parameters.Add(New SqlParameter("filtro", filtro))
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function

    Public Function Shipex_traerRutaConductorPorOpcionSelect(cliente As Int32, flag As Int32, opcionSelect As String) As DataTable
        Dim dt As DataTable = New DataTable()
        Using cnn As New SqlConnection(StringConexion)
            cnn.Open()
            Dim cmd As New SqlCommand("Shipex_traerRutaConductorPorOpcionSelect", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("idConductor", cliente))
            cmd.Parameters.Add(New SqlParameter("flag", flag))
            cmd.Parameters.Add(New SqlParameter("opcion", opcionSelect))
            Dim result As SqlDataReader = cmd.ExecuteReader
            dt.Load(result)
            result.Close()
            cnn.Close()
        End Using
        Return dt
    End Function

End Class
