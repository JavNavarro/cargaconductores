﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PuntosDeEntrega.aspx.vb" Inherits="PuntosDeEntrega" %>

<!doctype html>
<html lang="es">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<!-- Required meta tags -->
<link rel="stylesheet" href="css/style.css">
<!-- favicon -->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<title>Shipex</title>
</head>
<body>
    <form id="panel" runat="server">
<!--header-->
<nav class="navbar bg-corp" style="width:100%"> 
  <!-- Navbar content --> 
  <a class="navbar-brand img-fluid mx-auto d-block" href="#"><img src="images/logo-shipex.png" width="135" height="45" alt="Shipex"/></a> </nav>

<!-- info-->
	<!-- row -->
<div class="container-fluid">
    
    <div class="row">
        <div class="col-xs-10 ml-1">Fecha</div>
        <div class="col-xs-10 ml-1">
            <asp:Label runat="server" ID="lbl_fecha"></asp:Label></div>
        </div>

    <!-- row -->
    <div class="row">
        <div class="col-xs-10 ml-1">Cantidad de Puntos</div>
        <div class="col-xs-10 ml-1"><asp:Label runat="server" ID="lbl_countGuias"></asp:Label></div>
    </div>
    
    <!-- row -->
    <div class="row">
        <div class="col-xs-10 ml-1">Usuario</div>
        <div class="col-xs-10 ml-1"><asp:Label runat="server" ID="lbl_user"></asp:Label></div>
    </div>
    
    <div class="row">
        <asp:Button runat="server" ID="ruta" Text="Ver ruta" CssClass="btn-primary" visible="false"/>
        <asp:Button runat="server" ID="btn_actualizados" Text="Actualizados" CssClass="btn-primary" visible="false"/>
        <asp:Button runat="server" ID="btn_sinVer" Text="Ver no actualizados" CssClass="btn-primary" visible="false"/>
    </div>
    
    <div class="container col-lg-11">
        <div class="row">
            <label for="txt_guia">Ingrese datos:</label>
            <div class="col-lg-3">
                <asp:TextBox runat="server" ID="txt_filtro" CssClass="form-control"></asp:TextBox>
            </div>

            <div class="row">
                <asp:Button ID="btn_aceptar" runat="server" Text="Realizar busqueda"  CssClass="form-control btn btn-primary" />
            </div>
            <div class="row offset-sm-1">
                <asp:Button ID="btn_limpiar" runat="server" Text="Limpiar busqueda"   CssClass="form-control btn btn-success" />
            </div>
            
            <label class="offset-1" for="ddl_orden">Ordenar por:</label>
            <div class="col-lg-3">
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddl_orden" AutoPostBack="True" OnSelectedIndexChanged="ddl_orden_SelectedIndexChanged">
                    <asp:ListItem Value="0">Seleccione alguna opción</asp:ListItem>
                    <asp:ListItem Value="1">Dirección</asp:ListItem>
                    <asp:ListItem Value="2">Guias</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>

    <br />
    
<%--    <div class="container">
        
        <div class="row">
            
            <asp:Button ID="Button1" runat="server" Text="Volver"  CssClass="form-control btn btn-success" />

        </div>

    </div>--%>
    
  
  
  
  <!-- / info--> 
	
<!-- tabla-->

<div class="row">
    <div class="col-12">

        <asp:GridView ID="grd_Pedidos" runat="server" class="table-responsive"
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC"  BorderWidth="1px" 
                                     CellPadding="20" OnRowCommand="grd_Pedidos_RowCommand" width="100%">
            <Columns>             
                 <asp:TemplateField headerText="Guia" ItemStyle-Width="30%">
                    <itemtemplate>
                         <asp:Label id="Guia" Text='<%# Eval("nGuia") %>'  runat="server"  />
                        <asp:Button ID="btn_actualizarEstados" Text="Actualizar" runat="server" CssClass="form-control" CommandName="select"  CommandArgument='<%# Eval("actividad") %>' /> 
                       <%-- <asp:LinkButton ID="LinkBtn_Actividad" CommandName = "getAct" runat="server" Text='Actualizar estado' CommandArgument='<%# Eval("actividad") %>'/>--%>
                      <asp:Label id="estadoNombre" Text='<%# Eval("nombreEstado") %>'  runat="server"  />

                    </itemtemplate>
                     </asp:TemplateField>
            
                 <asp:TemplateField HeaderText="Datos de cliente" >
                    <itemtemplate>
                            <asp:Label id="direccion" Text='<%# Eval("direccion") %>'  runat="server" Font-Bold="true"/> 
                            <br> 
                        </br>
                        <asp:Label id="nombre" Text='<%# Eval("nombre") %>'  runat="server" />
                            <br>
                        </br>
                        <asp:Label id="numero" Text='<%# Eval("numero") %>'  runat="server" />
                            <br>
                        </br>

                       <asp:LinkButton ID="LinkBtn_enlace" CommandArgument='<%# Eval("enlaceGgl") %>' CommandName="EnlaceGoogle" runat="server">
    <asp:Image ID="Image1" runat="server" ImageUrl="images/google-map.png" style="border-width: 0px; max-width: 37px; max-height: 37px" />
</asp:LinkButton>
                       <asp:LinkButton ID="LinkBtn_enlace2" CommandArgument='<%# Eval("EnlaceWz") %>' CommandName="EnlaceWaze" runat="server">
    <asp:Image ID="Image2" runat="server" ImageUrl="images/waze.png" style="border-width: 0px; max-width: 37px; max-height: 37px" />
</asp:LinkButton>
                    </itemtemplate>
                     </asp:TemplateField>
                 
           <%--<asp:BoundField DataField="" Footertext="" HeaderText="Actividad" />--%>
                <asp:TemplateField Visible="false">
                    <itemtemplate>
                         <asp:Label id="estado" Text='<%# Eval("idEstado") %>'  runat="server" Visible ="false" />
                        <%-- <asp:LinkButton ID="LinkBtn_Actividad" CommandName = "getAct" runat="server" Text='Actualizar estado' CommandArgument='<%# Eval("actividad") %>'/>--%>
                    </itemtemplate>
                     </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
<%--  <table class="table mx-auto">
    <thead class="thead-light">
      <tr>
        <th scope="col">Guía</th>
        <th scope="col">Dirección</th>
        <th scope="col">Enlace</th>
        <th scope="col">Actividad</th>
        </tr>
      </thead>
    <tbody> 
      <tr>
        <td scope="row"><a href="#">12345</a></td>
        <td>Nombre de la calle 123</td>
        <td><img src="images/waze.png" width="32" height="32" alt="Waze" img-fluid/> <img src="images/google-map.png" width="32" height="32" alt="Google Map" img-fluid/></td>
        <td>Cerrar visita</td>
      </tr>
      <tr>
        <td scope="row"><a href="#">12345</a></td>
        <td>Nombre de la calle 123</td>
         <td><img src="images/waze.png" width="32" height="32" alt="Waze" img-fluid/> <img src="images/google-map.png" width="32" height="32" alt="Google Map" img-fluid/></td>
        <td>Cerrar visita</td>
      </tr>
      <tr>
        <td scope="row"><a href="#">12345</a></td>
        <td>Nombre de la calle 123</td>
         <td><img src="images/waze.png" width="32" height="32" alt="Waze" img-fluid/> <img src="images/google-map.png" width="32" height="32" alt="Google Map" img-fluid/></td>
        <td>Cerrar visita</td>
      </tr>
      </tbody>
  </table>--%>
       


</div>
	
	<!-- / tabla -->
	
	
  
</div>
<!-- Optional JavaScript --> 
<!-- jQuery first, then Popper.js, then Bootstrap JS --> 
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> 
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</form></body>
</html>

