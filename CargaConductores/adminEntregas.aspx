﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="adminEntregas.aspx.vb" Inherits="adminEntregas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
  <form id="panel" runat="server">
<!--header-->
<nav class="navbar bg-corp" style="width:100%"> 
  <!-- Navbar content --> 
  <a class="navbar-brand img-fluid mx-auto d-block" href="#"><img src="images/logo-shipex.png" width="135" height="45" alt="Shipex"/></a> </nav>

<!-- info-->
	<!-- row -->
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-10 ml-1">Fecha</div>
    <div class="col-xs-10 ml-1"><asp:Label runat="server" ID="lbl_fecha"></asp:Label></div>
  </div>
	<!-- row -->
	  <div class="row">
    <div class="col-xs-10 ml-1">Cantidad de Puntos</div>
    <div class="col-xs-10 ml-1"><asp:Label runat="server" ID="lbl_countGuias"></asp:Label></div>
  </div>
	<!-- row -->
  <div class="row">
    <div class="col-xs-10 ml-1">Usuario</div>
    <div class="col-xs-10 ml-1"><asp:Label runat="server" ID="lbl_user"></asp:Label></div>
  </div>
  <!-- / info--> 
	
<!-- tabla-->

<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="grd_Pedidos" runat="server" class="col-md-11"
                                    AutoGenerateColumns="False"  BackColor="White" BorderColor="#CCCCCC"  BorderWidth="1px" 
                                     CellPadding="20" OnRowCommand="grd_Pedidos_RowCommand" width="100%">
            <Columns>
           <asp:BoundField DataField="nGuia" Footertext="" HeaderText="Guia" />
           <asp:BoundField DataField="direccion" Footertext="" HeaderText="Dirección" />
<%--           <asp:BoundField DataField="links" Footertext="" HeaderText="Enlace" />--%>
              
             
                 <asp:BoundField DataField="nombreEstado" Footertext="" HeaderText="estado" />
           <%--<asp:BoundField DataField="" Footertext="" HeaderText="Actividad" />--%>
                <asp:TemplateField>
                    <itemtemplate>
                        <asp:Button ID="btn_actualizarEstados" Text="Actualizar" runat="server" CssClass="form-control" CommandName="select" CommandArgument='<%# Eval("actividad") %>' /> 
                       <%-- <asp:LinkButton ID="LinkBtn_Actividad" CommandName = "getAct" runat="server" Text='Actualizar estado' CommandArgument='<%# Eval("actividad") %>'/>--%>
                    </itemtemplate>
                     </asp:TemplateField>
                     <asp:TemplateField>
                    <itemtemplate>
                        <asp:Button ID="btn_eliminar" Text="Eliminar" runat="server" CssClass="form-control" CommandName="eliminar" CommandArgument='<%# Eval("nGuia") %>' /> 
                       <%-- <asp:LinkButton ID="LinkBtn_Actividad" CommandName = "getAct" runat="server" Text='Actualizar estado' CommandArgument='<%# Eval("actividad") %>'/>--%>
                    </itemtemplate>
                     </asp:TemplateField>
                
                </Columns>
            </asp:GridView>
        </div>
<%--  <table class="table mx-auto">
    <thead class="thead-light">
      <tr>
        <th scope="col">Guía</th>
        <th scope="col">Dirección</th>
        <th scope="col">Enlace</th>
        <th scope="col">Actividad</th>
        </tr>
      </thead>
    <tbody> 
      <tr>
        <td scope="row"><a href="#">12345</a></td>
        <td>Nombre de la calle 123</td>
        <td><img src="images/waze.png" width="32" height="32" alt="Waze" img-fluid/> <img src="images/google-map.png" width="32" height="32" alt="Google Map" img-fluid/></td>
        <td>Cerrar visita</td>
      </tr>
      <tr>
        <td scope="row"><a href="#">12345</a></td>
        <td>Nombre de la calle 123</td>
         <td><img src="images/waze.png" width="32" height="32" alt="Waze" img-fluid/> <img src="images/google-map.png" width="32" height="32" alt="Google Map" img-fluid/></td>
        <td>Cerrar visita</td>
      </tr>
      <tr>
        <td scope="row"><a href="#">12345</a></td>
        <td>Nombre de la calle 123</td>
         <td><img src="images/waze.png" width="32" height="32" alt="Waze" img-fluid/> <img src="images/google-map.png" width="32" height="32" alt="Google Map" img-fluid/></td>
        <td>Cerrar visita</td>
      </tr>
      </tbody>
  </table>--%>
</div>
	
	<!-- / tabla -->
	
	
  
</div>
<!-- Optional JavaScript --> 
<!-- jQuery first, then Popper.js, then Bootstrap JS --> 
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> 
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</form>
</body>
</html>
